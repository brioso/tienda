<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Cart_Item;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        /*$user = Auth::user()->id;
        $cart_db = Cart::where("user_id",$user)->first();
        if ( empty($cart_db) )
        {
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }

        $cart_item = new Cart_Item();
        $cart_item->cart_id=$cart_db->id;
        $cart_item->product_id=id;
        $cart_item->amount=1;
        $cart_item->save();

        $cart_item = Cart_Item::where([
            ['cart_id',$cart_db->id],
            ['product_id',id],
        ])->first();
        $cart_item->cantidad=cart[id]['amount'];
        $cart_item->update();*/
    }

    public function show(Request $request)
    {
        $min = empty($request->input('minPrice')) ? '0' : $request->input('minPrice');
        $max = empty($request->input('maxPrice')) ? '99999' : $request->input('maxPrice');

        $all = DB::select(DB::raw("select * from products where price >= $min AND price <= $max"));

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($all);
        $perPage = 6;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageItems, count($itemCollection), $perPage);
        $products->setPath($request->url());

        return view('products')->with('products', $products);
    }

    public function addToCart($id)
    {
        $product = Product::find($id);
        $user = Auth::user()->id;
        $cart_db = Cart::where("user_id", $user)->first();
        if (empty($cart_db)) {
            $cart_db = new Cart();
            $cart_db->user_id = $user;
            $cart_db->save();
        }
        if (!$product) {
            abort(404);
        }
        $cart = session()->get('cart');

        if (!$cart) {
            $cart = [
                $id => [
                    "name" => $product->name,
                    "amount" => 1,
                    "price" => $product->price,
                    "image" => $product->image]
            ];
            $cart_item = new Cart_Item();
            $cart_item->cart_id = $cart_db->id;
            $cart_item->product_id = $id;
            $cart_item->amount = 1;
            $cart_item->save();
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }

        if (isset($cart[$id])) {
            $cart[$id]['amount']++;
            $cart_item = Cart_Item::where([
                ['cart_id', $cart_db->id],
                ['product_id', $id],
            ])->first();
            /*dd($cart_item);*/
            $cart_item->amount = $cart[$id]['amount'];
            $cart_item->update();
            session()->put('cart', $cart);
            return redirect()->back()->with('Success', 'Product added to cart successfully!');
        }

        $cart[$id] = [
            "name" => $product->name,
            "amount" => 1,
            "price" => $product->price,
            "image" => $product->image
        ];
        $cart_item = new Cart_Item();
        $cart_item->cart_id = $cart_db->id;
        $cart_item->product_id = $id;
        $cart_item->amount = 1;
        $cart_item->save();
        session()->put('cart', $cart);
        return redirect()->back()->with('Success', 'Product added to cart successfully!');
    }

    public function cart()
    {
        return view('cart');
    }

    public function update(Request $request)
    {
        if ($request->id and $request->amount) {
            $cart = session()->get('cart');
            $cart[$request->id]["amount"] = $request->amount;

            $user = Auth::user()->id;
            $cart_db = Cart::where("user_id", $user)->first();
            $cart_item = Cart_Item::where([
                ['cart_id', $cart_db->id],
                ['product_id', $request->id],
            ])->first();
            $cart_item->amount = $request->amount;
            $cart_item->update();
            session()->put('cart', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

    public function remove(Request $request)
    {
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_db = Cart::where("user_id", $user)->first();
                $cart_item = Cart_Item::where([
                    ['cart_id', $cart_db->id],
                    ['product_id', $request->id],
                ])->first();
                $cart_item->delete();
                session()->put('cart', $cart);
            }
            session()->flash('success', 'Product removed successfully');
        }
    }

    public function purchase(Request $request)
    {
        $stockDisponible = false;
        $products = DB::table('cart_items')->get();

        for ($i = 0; $i < $products->count(); $i++) {
            $amount = $products[$i]->amount;
            $p = Product::find($products[$i]->product_id);

            if ($amount > $p->stock) {
                $stockDisponible = true;
            }
        }

        if ($stockDisponible == true) {
            session()->flash('fail', 'Products out of stock, sorry');
        } else {
            session()->remove('cart');
            session()->flash('success', 'Products in cart purchased successfully');
        }

        /*
        unset($cart);
        if ($request->id) {
            $cart = session()->get('cart');
            if (isset($cart[$request->id])) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_db = Cart::where("user_id", $user)->first();
                $cart_item = Cart_Item::where([
                    ['cart_id', $cart_db->id],
                    ['product_id', $request->id],
                ])->first();
                $cart_item->delete();
                session()->put('cart', $cart);
            }
        }

        session()->remove('cart');
        session()->flash('success', 'Products in cart purchased successfully');
*/

        /*if ($request->id) {
            $cart = session()->get('cart');
            $stockCorrect = true;

            //if ()

            if (isset($cart[$request->id]) && $stockCorrect) {
                unset($cart[$request->id]);
                $user = Auth::user()->id;
                $cart_db = Cart::where("user_id", $user)->first();
                $cart_items = Cart_items::where([
                    ['cart_id', $cart_db->id]
                ])->first();
                $cart_items->delete();
                session()->put('cart', $cart);

                session()->flash('success', 'Products in cart purchased successfully');
            }

        }*/
    }
}
