<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart_Item extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'cart_items';

    public function product()
    {
        $this->hasOne("App\Models\Product");
    }

    public function cart()
    {
        $this->belongsTo("App\Models\Cart");
    }
}
