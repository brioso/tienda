@extends('layouts.app')

@section('content')

    <header>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
                crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
                crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
                crossorigin="anonymous"></script>
    </header>
    <body>
    <div class="card-header">
        <form method="get" action="/products">
            <label>Min Price: </label>
            <input name="minPrice" type="text"/>
            <label>Max Price: </label>
            <input name="maxPrice" type="text"/>
            <input value="Search" type="submit" class="btn btn-warning"/>
        </form>
    </div>
    <div class="container">
        <div class="row">

            @foreach ($products as $product)
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">{{ $product->name }}</div>
                        <div class="card-body">
                            <li>{{$product->name}}</li>
                            <li>{{$product->price}}</li>

                            <img src="data:image/jpeg;base64,{!! stream_get_contents($product->image) !!}"/>
                            <p class="btn-holder"><a href="{{ url('add-to-cart/'.$product->id) }}" class="btn btn-warning btn-block text-center" role="button">añadir</a> </p>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>

    {{ $products->links() }}

    </body>

    <style>
        .card {
            margin: 1em;
        }

        li {
            list-style:none;
            margin: 5px;
        }
    </style>

@endsection
