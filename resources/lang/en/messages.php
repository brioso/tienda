<?php

return [
    'Name' => 'Name',
    'Surname' => 'Surname',
    'Email' => 'Email',
    'Confirm Password' => 'Confirm Password',
    'Password' => 'Password',
    'Already registered?' => 'Already registered?',
    'Register' => 'Register'
];

?>
