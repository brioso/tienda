<?php

return [
    'Name' => 'Nombre',
    'Surname' => 'Apellido',
    'Email' => 'Correo Electrónico',
    'Confirm Password' => 'Confirmar Contraseña',
    'Password' => 'Contraseña',
    'Already registered?' => '¿Ya te has registrado?',
    'Register' => 'Registrarse'
];

?>
