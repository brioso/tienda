<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'product_name',
            'price' => 12.5,
            'stock' => 5,
            'description' => 'product description',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'product_name2',
            'price' => 13.5,
            'stock' => 10,
            'description' => 'product description 2',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'Product 3',
            'price' => 50,
            'stock' => 25,
            'description' => 'Description of product 3',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'Product 4',
            'price' => 299.99,
            'stock' => 5,
            'description' => 'product description 4',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'product 5',
            'price' => 5.99,
            'stock' => 5,
            'description' => 'product description for product 5',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'product 6',
            'price' => 43,
            'stock' => 10,
            'description' => 'product description 6',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'Product 7',
            'price' => 66.70,
            'stock' => 25,
            'description' => 'Description of product 7',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);

        DB::table('products')->insert([
            'name' => 'Product 8',
            'price' => 299.99,
            'stock' => 5,
            'description' => 'product description 8',
            'image' => base64_encode(file_get_contents('./resources/images/gatillo.jpg')),
        ]);
    }
}
